#include <stdio.h>
#include <stdlib.h>
#define N 100
#include<string.h>
#include<math.h>

                            /* CONVERTISSEUR DE BASES NUMERIQUES */


    /*FONCTIONS */


       /* - CALCUL DE LA LONGUEUR DE LA CHAINE */

    int longueurchaine( char ch[]){

    int c =0;

        while(ch[c]!='\0'){

            c++;
        }
        return c;
    }

/*-------------------------------------------------------------------------*/

    /* - INVERSION DES CHIFFRES DANS LES CASES DU TABLEAU */

    void inverseChiffres(char ch[]){

    int i=0;

    int y = longueurchaine(ch)-1;

    char temp;/*VARIABLE*/

        for(i=0;i<longueurchaine(ch)/2; i++){

                temp= ch[i];

                ch[i]=ch[y];

                ch[y]=temp;

                y--;
        }
    }
/*---------------------------------------------------------------------------*/

    /* CONVERSION D'UN NOMBRE DECIMAL EN BINAIRE */

    void ConversionDecimalBinaire(int nb ,char bin[]){

    int i=0;

    int j=0;

    char temp;



        while(nb!=0){

            bin[i]=(nb%2==0) ? '0' : '1'; /*REMPLISSAGE DE LA CHAINE DE CARACTERES */

            i++;

            nb=nb/2;
        }

    bin[i]='\0';

        for (j=0; j<i/2; j++){

            temp=bin[j];            /*INVERSION DE LA CHAINE DE CARACTERES*/

            bin[j]=bin[i-1-j];

            bin[i-1-j]=temp;
        }

    }

/*------------------------------------------------------------------------------------------------


    /* CONVERSION D'UN NOMBRE DECIMAL EN OCTO*/

    int ConversionDecimalOctal(int nb){

    int octalnb = 0;

    int temp= 1;

        while (nb!= 0){

    	octalnb = octalnb + (nb % 8) * temp;
    	nb = nb / 8;
        temp = temp * 10;
    }

    return octalnb;
}
/*--------------------------------------------------------------------------------*/

    /* CONVERSION D'UN NOMBRE DECIMAL EN HEXADECIMAL*/


    void ConversionDecimalHexadecimal(int nb, char hex[]){

    int reste;

    int i=0;

    int j=0;

        while (nb!=0){

            reste=nb%16;

            if(reste<10){

                hex[i] = 48 + reste;

            }

            else{

                hex[i] = 55 + reste;

            }

            nb = nb/16;

            i++;

        }
        hex[i]='\0';

    inverseChiffres(hex);

    }

/*-----------------------------------------------------------------------------------*/

    /* CONVERSION NOMBRE OCTAL EN DECIMAL */


    int ConversionOctalDecimal(int octalnb){

    int nb=0;

    int i=0;

        while(octalnb != 0){

            nb= nb + (octalnb%10) * pow(8,i);

            i++;

            octalnb = octalnb / 10;

        }

            return nb;

    }

/*--------------------------------------------------------------------------------------------*/

    /* CONVERSION NOMBRE OCTAL EN BINAIRE*/


    int ConversionOctalBinaire(int octalnb){

    int decinb=0;/*DECIMAL*/

    int i = 0;

    int binnb=0;/*BINAIRE*/


    /* Boucle de conversion octal en decimal*/


    while(octalnb != 0){

        decinb = decinb+(octalnb%10)*pow(8,i);

        i++;

        octalnb = octalnb / 10;

    }

    //Reinitialisation de i

    i = 1;

    /* Boucle de conversion decimal en binaire*/

    while (decinb != 0){

        binnb= binnb+(decinb % 2)*i;

        decinb= decinb / 2;

        i = i * 10;

    }

    return binnb;

}

/*---------------------------------------------------------------------------------*/

    /* CONVERSION D'UN NOMBRE OCTAL EN HEXADECIMAL*/

    void ConversionOctalHexadecimal(int octalnb, char hex[]){

    int nb=0;/*decimal*/

    int i=0;

    int reste;

    /*boucle de conversion octal vers decimal*/

        while(octalnb != 0){

            nb= nb + (octalnb%10) * pow(8,i);

            i++;

            octalnb = octalnb / 10;

        }

    i=0; /*Reinitialistion de i*/


    /*Boucle de conversion decimal vers hexadecima*/

        while (nb!=0){

            reste=nb%16;

            if(reste<10){

                hex[i] = 48 + reste;

            }

            else{

                hex[i] = 55 + reste;

            }

            nb = nb/16;

            i++;

        }
        hex[i]='\0';

    inverseChiffres(hex);

    }

/*------------------------------------------------------------------------------------------------*/

    /* CONVERSION D'UN NOMBRE BINAIRE EN DECIMAL */

    int ConversionBinaireDecimal(int binnb){

    int nb = 0; /*Nombre decimal*/

    int i = 0;

    int reste;

    /*Boucle de conversion*/

        while (binnb!=0){

            reste = binnb%10;

            binnb /= 10;

            nb = nb+ reste*pow(2,i);

            i++;
        }

        return nb;
    }

/*------------------------------------------------------------------------------*/

    /* CONVERSION NOMBRE BINAIRE EN OCTAL*/

    int ConversionBinaireOctal(int binnb){

    int nb = 0; /*Nombre decimal*/

    int octalnb=0; /*Nombre octal*/

    int i = 0;

    int reste;

    /*Boucle de conversion binaire decimal*/

        while (binnb!=0){

            reste = binnb%10;

            binnb /= 10;

            nb = nb+ reste*pow(2,i);

            i++;
        }

         /* Reinitialistion de i */

         i=1;

     /*Boucle de conversion decimal octal*/

        while (nb!= 0){

            octalnb = octalnb + (nb % 8) *i;

            nb = nb / 8;

            i = i * 10;
        }

        return octalnb;
    }



/*---------------------------------------------------------------------------------------------*/

    /* CONVERSION D'UN NOMBRE BINAIRE EN HEXADECIMAL*/


    void ConversionBinaireHexadecimal(int binnb,char hex[]){

    int nb=0; /*Nombre decimal*/

    int i=0;

    int reste;

    int j=0;


    /*Boucle de conversion binaire decimal*/

        while (binnb!=0){

            reste = binnb%10;

            binnb /= 10;

            nb = nb+ reste*pow(2,i);

            i++;
        }

    i=0;/*Reinitialisation de i*/

    /*Boucle de conversion decimale hexadecimale*/

        while (nb!=0){

            reste=nb%16;

            if(reste<10){

                hex[i] = 48 + reste;

            }

            else{

                hex[i] = 55 + reste;
            }

            nb = nb/16;

            i++;

        }
        hex[i]='\0';

    inverseChiffres(hex);

    }

/*------------------------------------------------------------------------------------------*/

    /* CONVERSION D'UN NOMBRE HEXADECIMAL EN NOMBRE BINAIRE */

    int ConversionHexadecimalBinaire(char hex[]){

    int bin;

    int place;

    int i =0;

    bin = 0ll;

    place = 0ll;

    // CONVERSION


    for (i = 0; hex[i] != '\0'; i++) {

        bin = bin * place;

        switch (hex[i]) {
        case '0':
            bin += 0;
            break;
        case '1':
            bin += 1;
            break;
        case '2':
            bin += 10;
            break;
        case '3':
            bin += 11;
            break;
        case '4':
            bin += 100;
            break;
        case '5':
            bin += 101;
            break;
        case '6':
            bin += 110;
            break;
        case '7':
            bin += 111;
            break;
        case '8':
            bin += 1000;
            break;
        case '9':
            bin += 1001;
            break;
        case 'a':
        case 'A':
            bin += 1010;
            break;
        case 'b':
        case 'B':
            bin += 1011;
            break;
        case 'c':
        case 'C':
            bin += 1100;
            break;
        case 'd':
        case 'D':
            bin += 1101;
            break;
        case 'e':
        case 'E':
            bin += 1110;
            break;
        case 'f':
        case 'F':
            bin += 1111;
            break;

        }

        place = 10000;
    }

    return bin;

    }








/*-----------------------------------------------------------------------------------------------------------------*/

    /* CONVERSION NOMBRE HEXADECIMAL EN NOMBRE OCTAL*/

    int ConversionHexadecimalOctal(char hex[]){

    int octal;

    int bin;

        bin = ConversionHexadecimalBinaire(hex);

        octal= ConversionBinaireOctal( bin);

        return octal;

    }

/*-------------------------------------------------------------------------------------------------------

     /* CONVERSION D'UN NOMBRE HEXADECIMAL EN NOMBRE DECIMAL*/


    int ConversionHexadecimalDecimal(char hex[]){

    int bin;

    int dec;

        bin = ConversionHexadecimalBinaire(hex);

        dec = ConversionBinaireDecimal(bin);

        return dec;
    }










int main()
{

    int choix;

    int decimalnb;

    int octalnb;

    char hex[N];

    char bin[N];

    int binnb;

    int choice ='y';

    while(choice=='y'){

        choice = 'y';



        system("cls");



    printf("\t\t\t\t\t-----------------------------------------------------------------\n");
    printf("\t\t\t\t\t|\t\t CONVERTISSEUR NUMERIQUE\t\t\t|\n");
    printf("\t\t\t\t\t-----------------------------------------------------------------\n\n\n");




    printf("    Cet outil vous permet d'effectuer la conversion d'un nombre entre bases(binaire,decimale,octale,hexadecimale). \n\n ");

    printf("    Voulez vous effectuer une conversion : \n\n");

    printf("\t\t\t1-base decimale (base 10) vers base binaire(base 2).\n \n");

    printf("\t\t\t2-base decimale (base 10) vers base octale(base 8).\n \n");

    printf("\t\t\t3-base decimale (base 10) vers base hexadecimale(base 16).\n \n");

    printf("\t\t\t4-base binaire (base 2) vers base octale(base 8).\n \n");

    printf("\t\t\t5-base binaire (base 2) vers base decimale(base 10).\n \n");

    printf("\t\t\t6-base binaire (base 2) vers base hexadecimale(base 16).\n \n");

    printf("\t\t\t7-base octale (base 8) vers base binaire(base 2).\n \n");

    printf("\t\t\t8-base octale (base 8) vers base decimale(base 10).\n \n");

    printf("\t\t\t9-base octale (base 8) vers base hexadecimale(base 16).\n \n");

    printf("\t\t\t10-base hexadecimale (base 16) vers base binaire(base 2).\n \n");

    printf("\t\t\t11-base hexadecimale (base 16) vers base octale(base 8).\n \n");

    printf("\t\t\t12-base hexadecimale (base 16) vers base decimale(base 10).\n \n");

    printf("\tVotre choix : ");

    scanf("%d",&choix);

    system("cls");



        switch(choix){

        case 1 : case 2 : case 3 :

        printf("\n\tLes nombre decimaux ne sont composes que de chiffres de 0 a 9.\n");

        printf("\n\tEntrez votre nombre decimal:");

        scanf("%d",&decimalnb);

             break;

        case 4 : case 5 : case 6 :

        printf("\n\tLes nombre binaires ne sont composes que de chiffres 0 ou 1.\n");

        printf("\n\tEntrez votre nombre binaire:");

        scanf("%d",&binnb);

             break;

        case 7 : case 8 : case 9 :

        printf("\n\tLes nombre octaux ne sont composes que de chiffres allant  de 0 a 7.\n");

        printf("\n\tEntrez votre nombre octal:");

        scanf("%d",&octalnb);

             break;

        case 10 : case 11 : case 12 :

        printf("\n\tEntrez votre nombre hexadecimal:");

        scanf("%s",&hex);

        int i=0;

            while(hex[i]!='\0'){

                if(hex[i]<48||(hex[i]>57&& hex[i]<65)||(hex[i]>71&&hex[i]<97)||hex[i]>102){

                    printf("\n\tCe n'est pas un nombre hexadecimal\n");

                    printf("\n\tEntrez votre nombre hexadecimal:");

                    scanf("%s",&hex);
                }
                i++;
            }

             break;
        }


        if(choix==1){

            ConversionDecimalBinaire( decimalnb,bin);

            printf("\n\tEquivalent binaire:%s\n",bin);



        }

        if(choix==2){

            printf("\n\tEquivalent Octal:%d\n", ConversionDecimalOctal(decimalnb));


        }

        if(choix==3){

            ConversionDecimalHexadecimal(decimalnb, hex);

            printf("\n\tEquivalent hexadecimal:%s\n",hex);
        }

        if(choix==4){

            printf("\n\tEquivalent octal:%d\n",ConversionBinaireOctal(binnb));

        }

        if(choix==5){

             printf("\n\tEquivalent decimal:%d\n",ConversionBinaireDecimal( binnb));

        }

        if(choix==6){

            ConversionBinaireHexadecimal(binnb,hex);

            printf("\n\tEquivalent hexadecimal:%s\n",hex);

        }


        if(choix==7){

           printf("\n\tEquivalent binaire:%d\n",ConversionOctalBinaire(octalnb));

        }

        if(choix==8){

           printf("\n\tEquivalent decimal:%d\n",ConversionOctalDecimal( octalnb));

        }

        if(choix==9){

            ConversionOctalHexadecimal(octalnb,hex);

            printf("\n\tEquivalent hexadecimal:%s\n",hex);

        }

        if(choix==10){

            printf("\n\tEquivalent binaire:%d\n",ConversionHexadecimalBinaire( hex));

        }

        if(choix==11){

            printf("\n\tEquivalent octal:%d\n",ConversionHexadecimalOctal( hex));

        }

        if(choix==12){

            printf("\n\tEquivalent decimal:%ld\n",ConversionHexadecimalDecimal(hex));

        }

        printf("\n\tSouhaitez vous recommencer? y/n ");

        scanf("%s", &choice);

    }
    return 0;
}
